package no.noroff.nicholas.Original;

import java.util.Random;

public class Sensor {
    public static final double OFFSET = 16;
// 1 method doing the same thing more or less.. deleted popNext.. and just added "OFFSET to equation
// "S"ingle responsibility, DRY..KISS..YAGNI..
    public double popNextPressurePsiValue(){
        double pressureTelemetryValue;
        pressureTelemetryValue = samplePressure();
        return OFFSET + pressureTelemetryValue;
    }

    private double samplePressure() {
        Random basicRandomNumbersGenerator = new Random();
        double pressureTelemetryValue = 6 * basicRandomNumbersGenerator.nextDouble() * basicRandomNumbersGenerator.nextDouble();
        return pressureTelemetryValue;
    }
}
