package no.noroff.nicholas.Original;

public class Alarm {
    private final double LowPressureThreshold = 17;
    private final double HighPressureThreshold = 21;

 // Dependency issue, open close   solved with an interface..
    Sensor sensor = new Sensor();
    boolean alarmOn = false;

    public void check(){
        double psiPressureValue = sensor.popNextPressurePsiValue();

        if(psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue){
            alarmOn = true;
        }
    }

    public boolean isAlarmOn(){
        return alarmOn;
    }
}
