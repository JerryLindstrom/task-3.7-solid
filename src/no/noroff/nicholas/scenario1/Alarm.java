package no.noroff.nicholas.scenario1;

public class Alarm {
    private final double LowPressureThreshold = 17;
    private final double HighPressureThreshold = 21;

    private boolean alarmOn = false;

    private void check(test sensor){
        double psiPressureValue = sensor.samplePressure();
        if(psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue){
            alarmOn = true;
        }
    }

    public boolean isAlarmOn(test sensor) {
        check(sensor);
        return alarmOn; }
}
