package no.noroff.nicholas.scenario1;

import java.util.Random;

public class Sensor implements test{
    private static final double OFFSET = 16;
    private static final double MULTIPLIER = 7;

    public double samplePressure() {
        Random basicRandomNumbersGenerator = new Random();
        return MULTIPLIER * basicRandomNumbersGenerator.nextDouble() * basicRandomNumbersGenerator.nextDouble() + OFFSET;
    }
}
